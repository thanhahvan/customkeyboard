package vkey.android.vkeyboard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_scroll.*
import vkey.android.customkeyboard.keyboard.CustomKeyboardView

class ScrollActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scroll)
        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et3 )
        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et4 )
        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et1 )

    }

    override fun onBackPressed() {

        if (keyboard.isExpanded) {
            keyboard.toggleKeyboardView()
        } else {
            super.onBackPressed()
        }
    }
}
