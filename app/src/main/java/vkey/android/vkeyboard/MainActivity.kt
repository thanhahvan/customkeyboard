package vkey.android.vkeyboard

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import vkey.android.customkeyboard.keyboard.CustomKeyboardView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        //keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et2 )

        // keyboard is the bind view from activity_main. Ctrl + click the keyboard to see the xml source.
        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et3 )

        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et4 )

        keyboard.registerEditText(CustomKeyboardView.KeyboardType.NUMBER, et1 )

        // đi tới demo full màn hình
        goToFull.setOnClickListener{
            startActivity(Intent(this, FullscreenActivity::class.java))
        }
        // demo scroll view
        gotoLinear.setOnClickListener{
            startActivity(Intent(this, ScrollActivity::class.java))
        }
    }

    override fun onBackPressed() {

        // if keyboard is show => hide it
        if (keyboard.isExpanded) {
            keyboard.toggleKeyboardView()
        } else {
            super.onBackPressed()
        }
    }
}
