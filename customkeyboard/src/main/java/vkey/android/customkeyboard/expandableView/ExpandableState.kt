package vkey.android.customkeyboard.expandableView

enum class ExpandableState {
    COLLAPSED,
    COLLAPSING,
    EXPANDED,
    EXPANDING
}