package vkey.android.customkeyboard.expandableView


interface ExpandableStateListener {
    fun onStateChange(state: ExpandableState)
}