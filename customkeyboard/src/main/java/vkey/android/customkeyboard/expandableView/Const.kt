package vkey.android.customkeyboard.expandableView

import vkey.android.customkeyboard.toDp

val KEYBOARD_HEIGHT = 510.toDp
val KEYBOARD_HEIGHT_FLOAT = 510.toDp.toFloat()
const val ANIMATE_DURATION =  200L
const val ANIMATE_DELAY =  300L