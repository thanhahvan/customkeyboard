package vkey.android.customkeyboard.expandableView

import android.content.Context
import android.util.AttributeSet
import android.view.View
import vkey.android.customkeyboard.ResizableLayout

abstract class ExpandableView(
    context: Context, attr: AttributeSet
) :
    ResizableLayout(context, attr) {

    private var state: ExpandableState? = null
    private val stateListeners = ArrayList<ExpandableStateListener>()

    val isExpanded: Boolean
        get() = state === ExpandableState.EXPANDED

    init {
        state = ExpandableState.EXPANDED
        visibility = View.INVISIBLE
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        toggleKeyboardView()
    }

    fun registerListener(listener: ExpandableStateListener) {
        stateListeners.add(listener)
    }

    fun toggleKeyboardView() {

        if (state !== ExpandableState.EXPANDING && state !== ExpandableState.COLLAPSING) {
            when (state) {
                ExpandableState.EXPANDED -> {
                    resetParentViewState()
                    animate()
                        .translationY(KEYBOARD_HEIGHT_FLOAT)
                        .setDuration(ANIMATE_DURATION)
                        .withStartAction{
                            updateState(ExpandableState.COLLAPSING)
                        }
                        .withEndAction {
                            updateState(ExpandableState.COLLAPSED)
                            visibility = View.INVISIBLE
                        }
                        .start()
                }

                ExpandableState.COLLAPSED -> {
                    visibility = View.VISIBLE
                    animate()
                        .translationY(0f)
                        .setDuration(ANIMATE_DURATION)
                        .withStartAction{
                            updateState(ExpandableState.EXPANDING)
                        }
                        .withEndAction {
                            updateState(ExpandableState.EXPANDED)
                        }
                        .start()
                }

                else -> return
            }
        }
    }

    protected fun updateState(nextState: ExpandableState) {
        state = nextState
        for (listener in stateListeners) {
            listener.onStateChange(nextState)
        }
    }

    abstract fun resetParentViewState()

    abstract override fun configureSelf()
}
