package vkey.android.customkeyboard.keyboard

import vkey.android.customkeyboard.keyboard.controllers.KeyboardController


interface KeyboardListener {

    fun characterClicked(c: Char)

    fun specialKeyClicked(key: KeyboardController.SpecialKey)

}