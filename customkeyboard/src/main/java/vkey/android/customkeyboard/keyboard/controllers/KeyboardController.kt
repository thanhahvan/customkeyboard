package vkey.android.customkeyboard.keyboard.controllers

import android.view.inputmethod.InputConnection
import vkey.android.customkeyboard.keyboard.KeyboardListener

class KeyboardController(private val inputConnection: InputConnection) {

    private var mListener : KeyboardListener? = null
    private var cursorPosition: Int = 0
    private var inputText = ""

    companion object {

        private fun deleteCharacter(text: String, index: Int): String {
            return StringBuilder(text).deleteCharAt(index).toString()
        }

        private fun addCharacter(text: String, addition: Char, index: Int): String {
            return text.substring(0, index) + addition + text.substring(index)
        }
    }

    @Synchronized fun onKeyStroke(c: Char) {
        updateMembers()
        handleKeyStroke(c)
        mListener?.characterClicked(c)
    }

    @Synchronized fun onKeyStroke(key: SpecialKey) {
        updateMembers()
        handleKeyStroke(key)
        mListener?.specialKeyClicked(key)
    }

    fun registerListener(listener: KeyboardListener) {
        mListener = listener
    }

    private fun updateMembers() {
        val beforeText = beforeCursor()
        val afterText = afterCursor()
        cursorPosition = beforeText.length
        inputText = beforeText + afterText
    }

    private fun beforeCursor(): String {
        return inputConnection.getTextBeforeCursor(100, 0).toString()
    }

    private fun afterCursor(): String {
        return inputConnection.getTextAfterCursor(100, 0).toString()
    }

    private fun deleteCharacter() {
        if (cursorPosition == 0) {
            return
        }
        inputConnection.deleteSurroundingText(1, 0)
        inputText = deleteCharacter(inputText, --cursorPosition)
    }

    private fun addCharacter(c: Char) {
        if (cursorPosition >= maxCharacters()) {
            return
        }
        inputConnection.commitText(c.toString(), 1)
        inputText = if (cursorPosition++ >= inputText.length) { inputText + c } else {
            addCharacter(inputText, c, (cursorPosition - 1))
        }
    }

    private  fun handleKeyStroke(c: Char) {
        addCharacter(c)
    }

    private  fun handleKeyStroke(key: SpecialKey) {

        when(key){
            SpecialKey.BACKSPACE -> {
                deleteCharacter()
            }
        }
    }

    private  fun maxCharacters(): Int {
        return Int.MAX_VALUE
    }

    enum class SpecialKey {
        BACKSPACE,
        CLOSE
    }
}
