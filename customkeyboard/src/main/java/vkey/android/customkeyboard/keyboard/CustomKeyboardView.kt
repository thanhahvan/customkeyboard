package vkey.android.customkeyboard.keyboard

import android.app.Activity
import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.EditText
import android.widget.LinearLayout.VERTICAL
import androidx.core.content.ContextCompat
import vkey.android.customkeyboard.*
import vkey.android.customkeyboard.expandableView.*
import vkey.android.customkeyboard.keyboard.controllers.KeyboardController
import vkey.android.customkeyboard.keyboard.layouts.KeyboardLayout
import vkey.android.customkeyboard.keyboard.layouts.NumberKeyboardLayout
import java.util.*
import kotlin.math.abs


class CustomKeyboardView(context: Context, attr: AttributeSet) : ExpandableView(context, attr) {

    private var fieldInFocus: EditText? = null
    private val keyboards = HashMap<EditText, KeyboardLayout?>()
    private var latestFieldY = 0
    private var latestTranslation = 0f
    private lateinit var inputConnection: InputConnection
    private val keyboardListener: KeyboardListener

    init {

        setBackgroundColor(ContextCompat.getColor(context, R.color.custom_keyboard_background))

        keyboardListener = object : KeyboardListener {

            override fun characterClicked(c: Char) {
                // don't need to do anything here
            }

            override fun specialKeyClicked(key: KeyboardController.SpecialKey) {
                if (key === KeyboardController.SpecialKey.CLOSE) {
                    toggleKeyboardView()
                }
            }
        }
        registerListener(object : ExpandableStateListener {

            override fun onStateChange(state: ExpandableState) {

                if (state == ExpandableState.COLLAPSING) {
                    resetParentViewState()
                }
                if (state == ExpandableState.EXPANDING) {
                    postDelayed({ predictMoveParentView() }, 100)
                }
            }
        })
        setOnClickListener {}
    }

    public fun deRegisterEditText(field: EditText) {
        field.enableSoftInput()
        keyboards.remove(field)
    }

    public fun registerEditText(field: EditText) {
        registerEditText(KeyboardType.QWERTY, field)
    }

    public fun registerEditText(type: KeyboardType, field: EditText) {

        if (!field.isEnabled) {
            return  // disabled fields do not have input connections
        }
        field.disableSoftInput()

        inputConnection = field.onCreateInputConnection(EditorInfo())
        keyboards[field] = createKeyboardLayout(type, inputConnection)
        keyboards[field]?.registerListener(keyboardListener)

        field.onFocusChangeListener = OnFocusChangeListener { _: View, hasFocus: Boolean ->

            if (hasFocus) {

                // hide system keyboard
                hideSystemKeyboard(context, field)

                fieldInFocus = field

                renderKeyboard()

                if (!isExpanded) {
                    toggleKeyboardView()
                }
            } else if (!hasFocus && isExpanded) {
                for (editText in keyboards.keys) {
                    if (editText.hasFocus()) {
                        updateParentView()
                        return@OnFocusChangeListener
                    }
                }
                resetViewStateInstantly()
            } else if (!hasFocus && !isExpanded) {
                resetParentViewState()
            }
        }

        field.setOnClickListener {
            if (!isExpanded) {
                toggleKeyboardView()
            }
        }
    }


    public fun autoRegisterEditTexts(rootView: ViewGroup) {
        registerEditTextsRecursive(rootView)
    }

    private fun registerEditTextsRecursive(view: View) {
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                registerEditTextsRecursive(view.getChildAt(i))
            }
        } else if (view is EditText) {
            when (view.inputType) {
                InputType.TYPE_CLASS_NUMBER -> {
                    registerEditText(KeyboardType.NUMBER, view)
                }
                else -> {
                    registerEditText(KeyboardType.QWERTY, view)
                }
            }
        }
    }

    public fun unregisterEditText(field: EditText?) {
        keyboards.remove(field)
    }

    public fun clearEditTextCache() {
        keyboards.clear()
    }

    private fun renderKeyboard() {
        removeAllViews()
        val keyboard: KeyboardLayout? = keyboards[fieldInFocus]
        keyboard?.let {
            it.orientation = VERTICAL
            it.createKeyboard(measuredWidth.toFloat())
            addView(keyboard)
        }
    }

    private fun createKeyboardLayout(type: KeyboardType, ic: InputConnection): KeyboardLayout? {
        return NumberKeyboardLayout(context, createKeyboardController(type, ic))
    }

    private fun createKeyboardController(
        type: KeyboardType,
        ic: InputConnection
    ): KeyboardController? {
        return KeyboardController(ic)
    }

    override fun configureSelf() {
        renderKeyboard()
        translateParentView()
    }

    private fun resetViewStateInstantly() {
        updateState(ExpandableState.COLLAPSED)
        translationY = KEYBOARD_HEIGHT_FLOAT
        visibility = View.INVISIBLE
        fieldInFocus?.run {
            var fieldParent = this.parent
            while (fieldParent !== null) {
                if (fieldParent is View) {
                    fieldParent.translationY = 0f
                    break
                }
                fieldParent = fieldParent.parent
            }
        }
    }

    private fun handleFocusChange(editText: EditText) {
        editText.run {
            var fieldParent = this.parent
            while (fieldParent !== null) {
                if (fieldParent is View) {

                    val fieldLocation = IntArray(2)
                    editText.getLocationOnScreen(fieldLocation)

                    val keyboardLocation = IntArray(2)
                    this@CustomKeyboardView.getLocationOnScreen(keyboardLocation)

                    val fieldY = fieldLocation[1] // Y of the edit text

                    val keyboardY = keyboardLocation[1] - editText.height

                    // Y of custom keyboard + the edit text height itself
                    if (fieldY > keyboardY) { // edit text is under keyboard

                        val deltaY = (fieldY - keyboardY)  // the gap between them.
                        fieldParent
                            .animate()
                            .translationY(translationY - abs(deltaY.toFloat()))
                            .setDuration(ANIMATE_DURATION)
                            .withEndAction { updateLatestField() }
                            .start()
                    }
                    break
                }
                fieldParent = fieldParent.parent
            }
        }
    }
    private fun predictMoveParentView() {

        fieldInFocus?.run {
            var fieldParent = this.parent
            while (fieldParent !== null) {
                if (fieldParent is View) {

                    val fieldLocation = IntArray(2)
                    this.getLocationOnScreen(fieldLocation)
                    val keyboardY = getScreenHeight() - this@CustomKeyboardView.height
                    val fieldY = fieldLocation[1] // Y of the edit text
                    // Y of custom keyboard + the edit text height itself
                    if (fieldY > keyboardY) { // edit text is under keyboard

                        val deltaY = (fieldY - keyboardY)  // the gap between them.
                        fieldParent.animate()
                            .translationY(latestTranslation - abs(deltaY.toFloat()))
                            .setDuration(ANIMATE_DURATION)
                            .withEndAction { updateLatestField() }
                            .start()
                    }
                    break
                }
                fieldParent = fieldParent.parent
            }
        }
    }

    private fun translateParentView() {

        fieldInFocus?.run {
            var fieldParent = this.parent
            while (fieldParent !== null) {
                if (fieldParent is View) {

                    val fieldLocation = IntArray(2)
                    this.getLocationOnScreen(fieldLocation)

                    val keyboardLocation = IntArray(2)
                    this@CustomKeyboardView.getLocationOnScreen(keyboardLocation)

                    val fieldY = fieldLocation[1] // Y of the edit text

                    val keyboardY = keyboardLocation[1] - this.height

                    // Y of custom keyboard + the edit text height itself
                    if (fieldY > keyboardY) { // edit text is under keyboard

                        val deltaY = (fieldY - keyboardY)  // the gap between them.
                        fieldParent
                            .animate()
                            .translationY(-abs(deltaY.toFloat()))
                            .setDuration(ANIMATE_DURATION)
                            .withEndAction { updateLatestField() }
                            .start()
                    }
                    break
                }
                fieldParent = fieldParent.parent
            }
        }
    }

    private fun updateParentView() {

        fieldInFocus?.run {
            val fieldParent = this.parent
            while (fieldParent !== null) {
                if (fieldParent is View) {

                    val fieldLocation = IntArray(2)
                    this.getLocationOnScreen(fieldLocation)

                    val fieldY = fieldLocation[1]
                    val deltaY = fieldY - latestFieldY

                    // text is above the current one, need to move layout down a little
                    if (deltaY < 0 && fieldParent.translationY < 0) {

                        // if the current parent translation state is greater
                        // layout parent is push higher. then we reduce the height a little
                        if (deltaY > fieldParent.translationY) {
                            fieldParent
                                .animate()
                                .translationY(fieldParent.translationY - deltaY)
                                .setDuration(ANIMATE_DURATION)
                                .withEndAction { updateLatestField() }
                                .start()
                        } else { // mean that parent layout need to be reset since gap between them is greater than parent's current translation
                            fieldParent
                                .animate()
                                .translationY(0f)
                                .setDuration(ANIMATE_DURATION)
                                .withEndAction { updateLatestField() }
                                .start()
                        }
                        // text is under the current one. need to move layout up a little
                    }
                    break
                }
            }
        }
    }

    private fun updateLatestField() {
        val fieldLocation = IntArray(2)
        this.getLocationOnScreen(fieldLocation)
        latestFieldY = fieldLocation[1]
    }

    override fun resetParentViewState() {
        fieldInFocus?.run {
            latestFieldY = 0
            val fieldParent = this.parent
            if (fieldParent !== null && fieldParent is View) {
                fieldParent.animate()
                    .translationY(0f)
                    .setDuration(ANIMATE_DURATION)
                    .start()

            }
        }
    }

    enum class KeyboardType {
        NUMBER,
        QWERTY
    }

    private fun getScreenHeight(): Int {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager
            .defaultDisplay
            .getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }
}
